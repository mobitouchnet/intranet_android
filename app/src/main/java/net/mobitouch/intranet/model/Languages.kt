package net.mobitouch.intranet.model

data class Languages(
    val language : String,
    val proficiency: String,
    val speakigProficiency: String
)