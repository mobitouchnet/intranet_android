package net.mobitouch.intranet.ui.dashboard

import android.net.Uri

interface DashboardView{
        fun startLoginActivity()
        fun showImage(link:String)
}