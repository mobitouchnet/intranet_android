package net.mobitouch.intranet.ui.dashboard.profile.profile_fragment

import android.util.Log
import net.mobitouch.intranet.model.DataResponse
import net.mobitouch.intranet.model.ProfileSave
import net.mobitouch.intranet.model.User
import net.mobitouch.intranet.repository.local.TokenDatabse
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import net.mobitouch.intranet.repository.remote.Api
import retrofit2.Callback
import javax.inject.Inject

class ProfilePresenter @Inject constructor(val db: TokenDatabse, val retrofit: Retrofit) {
    val api = retrofit.create(Api::class.java)

    lateinit var profileView: ProfileView
    fun attach(profileView: ProfileView) {
        this.profileView = profileView
    }

    fun userProfile() {
        val token = db.tokenDao().getAll().last().token
        api.getUser(token)
            .enqueue(object : retrofit2.Callback<DataResponse<User>> {

                override fun onResponse(call: Call<DataResponse<User>>, response: Response<DataResponse<User>>) {
                    response.body()?.data?.let {
                        val name: String = it.name
                        val lastName: String = it.lastName
                        val position: String = it.position
                        val workingSince: String = it.workingSince
                        val birthdayDate: String = it.birthdayDate
                        val interests: String = it.interests
                        val phone: String = it.phoneNumber
                        val role: String = it.phoneNumber
                        val photo: String = it.photo

                        profileView.showName(name)
                        profileView.showLastName(lastName)
                        profileView.showPosition(position)
                        profileView.showWorkingSince(workingSince)
                        profileView.showBirthdayDate(birthdayDate)
                        profileView.showInterests(interests)
                        profileView.showPhoneNumber(phone)
                        profileView.showRole(role)
                        profileView.showPhoto(photo)

                    }
                }

                override fun onFailure(call: Call<DataResponse<User>>, t: Throwable) {
                }
            }
            )
    }

    fun save(
       profileSave : ProfileSave
    ) {
        val token = db.tokenDao().getAll().last().token
        api.postUser(token, profileSave.gethashMap())
            .enqueue(object : Callback<DataResponse<String>> {
                override fun onResponse(call: Call<DataResponse<String>>, response: Response<DataResponse<String>>) {
                    if (response.code() == 200) {
                    Log.e("Message", "Zapisano")

                    } else Log.e("error", "nie zapisano")
                }

                override fun onFailure(call: Call<DataResponse<String>>, t: Throwable) {

                }
            })
    }
}
