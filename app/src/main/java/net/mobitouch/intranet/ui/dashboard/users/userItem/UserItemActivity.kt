package net.mobitouch.intranet.ui.dashboard.users.userItem

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.fragment_user_item.*
import kotlinx.android.synthetic.main.user_list_item.*
import net.mobitouch.intranet.R
import net.mobitouch.intranet.model.User
import javax.inject.Inject

class UserItemActivity : AppCompatActivity(), UserItemView {


    @Inject
    lateinit var presenter: UserItemPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(net.mobitouch.intranet.R.layout.fragment_user_item)
        AndroidInjection.inject(this)
        presenter.attach(this)
        val user : User = intent.getParcelableExtra("user")
        showUser(user)

    }

    @SuppressLint("SetTextI18n")
    override fun showUser(user: User) {
        userName.text = "${user.name} ${user.lastName}"
        userPosition.text = user.position
        userPhoneNumber.text = user.phoneNumber
        userWorkSince.text = user.workingSince
        userBirthdayDate.text = user.birthdayDate
        userInterests.text = user.interests
        userRole.text = user.companyRoles
        val url = user.photo
        Glide
            .with(this)
            .load("http://dev.intra.mobitouch.net$url")
            .placeholder(R.drawable.placeholder33_round)
            .into(profilePhoto)
    }


}