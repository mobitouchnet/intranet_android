package net.mobitouch.intranet.model

data class Workexperience(
    val period : String,
    val name: String,
    val position: String
)