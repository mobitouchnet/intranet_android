package net.mobitouch.intranet.ui.dashboard

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_bottomsheet.*
import net.mobitouch.intranet.R
import net.mobitouch.intranet.ui.dashboard.home.HomeFragment
import net.mobitouch.intranet.ui.dashboard.profile.profile_home.ProfileHomeFragment
import net.mobitouch.intranet.ui.dashboard.absence.MyAbsenceFragment
import net.mobitouch.intranet.ui.dashboard.users.UsersFragment
import net.mobitouch.intranet.ui.login.LoginActivity
import javax.inject.Inject


class BottomNavigationDrawerFragment : BottomSheetDialogFragment(), DashboardView {

    override fun startLoginActivity() {
        val intent = Intent(this.context, LoginActivity::class.java)
        startActivity(intent)
    }

    override fun showImage(link: String) {
        Glide.with(this).load("http://dev.intranet.mobitouch.net/$link").into(userPhoto)
    }

    @Inject
    lateinit var presenter: DashboardPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_bottomsheet, container, false)
        AndroidSupportInjection.inject(this)
        presenter.attach(this)
        return view
    }

    private lateinit var clickListener: ClickListener
    fun setClickListener(clickListener: ClickListener) {
        this.clickListener = clickListener
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        navigation_view.setNavigationItemSelectedListener { menuItem ->
            closeFragment()
            if (clickListener != null) {
                clickListener.onItemClick(menuItem.itemId)
            }
            when (menuItem.itemId) {
                R.id.home -> {
                    activity?.supportFragmentManager?.beginTransaction()?.replace(
                        R.id.fullscreen_content,
                        HomeFragment(),
                        "home"
                    )?.commit()
                }
                R.id.absence -> {
                    activity?.supportFragmentManager?.beginTransaction()?.replace(
                        R.id.fullscreen_content,
                        MyAbsenceFragment(),
                        "absence"
                    )?.commit()
                }
                R.id.users -> {
                    activity?.supportFragmentManager?.beginTransaction()?.replace(
                        R.id.fullscreen_content,
                        UsersFragment(),
                        "users"
                    )?.commit()
                }
                R.id.profile -> {
                    activity?.supportFragmentManager?.beginTransaction()?.replace(
                        R.id.fullscreen_content,
                        ProfileHomeFragment(),
                        "profile"
                    )?.commit()
                }

                R.id.logout -> presenter.logout()
            }
            true
        }
    }

    fun closeFragment() {
        activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
    }

    interface ClickListener {
        fun onItemClick(fragmentId: Int)
    }
//
//    fun changeToDefaultIcon(iconId: Int) {
//        if (activity is DashboardActivity) {
//            val a = activity as DashboardActivity
//            a.onItemClick(iconId)
//        }
//    }
}