package net.mobitouch.intranet.ui.dashboard.users

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_users.*

import net.mobitouch.intranet.R
import net.mobitouch.intranet.model.User
import net.mobitouch.intranet.ui.dashboard.users.userItem.UserItemActivity
import javax.inject.Inject


class UsersFragment : Fragment(), UsersView {


    override fun showAllUsers(usersList: List<User>) {
        adapter.setUserList(usersList.toMutableList())
    }

    @Inject
    lateinit var presenter: UsersPresenter

    @Inject
    lateinit var adapter: UsersListAdapter


    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_users, parent, false)
        AndroidSupportInjection.inject(this)
        presenter.attach(this)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycleViewUsers.layoutManager = LinearLayoutManager(recycleViewUsers.context)
        adapter.setOnClickListener { user ->
            val intent = Intent(activity, UserItemActivity::class.java)
            intent.putExtra("user", user)
            startActivity(intent)
        }
        recycleViewUsers.adapter = adapter
        presenter.usersList()


    }


}
