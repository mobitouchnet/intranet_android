package net.mobitouch.intranet.ui.dashboard.home

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_users.*
import net.mobitouch.intranet.R
import javax.inject.Inject



class HomeFragment : Fragment(), HomeView {
    override fun showYearly(textMessage: String) {
        textNumberYearProposal.setText(textMessage)
    }

    override fun showMonthly(textMessage: String) {
        textNumberMonthProposal.setText(textMessage)
    }

    override fun showAll(textMessage: String) {
        textNumberOfProposal.setText(textMessage)
    }


    @Inject
    lateinit var presenter: HomePresenter

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_home, parent, false)
        AndroidSupportInjection.inject(this)
        presenter.attach(this)

        presenter.dashboard()

        return view

    }

}