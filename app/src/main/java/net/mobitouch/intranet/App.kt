package net.mobitouch.intranet

import com.facebook.stetho.Stetho
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import net.mobitouch.intranet.di.AppComponent
import net.mobitouch.intranet.di.DaggerAppComponent


class App : DaggerApplication() {
    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)


    }
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val builder : AppComponent.Builder = DaggerAppComponent.builder()
        builder.seedInstance(this)
        return builder.build().androidInjector()
    }

}