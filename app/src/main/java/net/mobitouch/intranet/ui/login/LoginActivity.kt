package net.mobitouch.intranet.ui.login

import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_login.*
import net.mobitouch.intranet.R
import net.mobitouch.intranet.BuildConfig
import net.mobitouch.intranet.ui.dashboard.DashboardActivity


import javax.inject.Inject

class LoginActivity : AppCompatActivity(), LoginView {
    override fun showInccorectLoginMessage(errorMessage: Int) {
        email.error = getString(errorMessage)

    }

    override fun showInccorectLoginMessage(errorMessage: String) {
        email.error = getString(R.string.incorrect_login)
    }

    override fun showIncorectPasswordMessage(errorMessage: String) {
        password.error = getString(R.string.incorrect_password)
    }

    override fun startHomeActivity() {
        val intent = Intent(this, DashboardActivity::class.java)
        startActivity(intent)
    }

    override fun showErrorMessage(errorMessage: String) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show()
    }

    @Inject
    lateinit var presenter: LoginPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        presenter.attach(this)
        val id: Int = R.string.app_name
        val text = getString(id)
        setContentView(net.mobitouch.intranet.R.layout.activity_login)
        if (BuildConfig.DEBUG) {
            password.setText("1qazXSW@")
            email.setText("ndzimiera@mobitouch.net")
        }

        loginButton.setOnClickListener {
            loginClick()
        }

    }

    fun loginClick() {
        presenter.login(email.text.toString(), password.text.toString())
        val progress = progressBar
        progress.visibility = View.VISIBLE
        
        progress.progress = 1500

    }
}



