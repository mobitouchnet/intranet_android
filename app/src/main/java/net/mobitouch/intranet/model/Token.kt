package net.mobitouch.intranet.model

data class Token (
    val token : String
)