package net.mobitouch.intranet.ui.dashboard

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Index
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import dagger.android.AndroidInjection
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_password.*
import kotlinx.android.synthetic.main.fragment_profile.*
import net.mobitouch.intranet.R
import net.mobitouch.intranet.ui.dashboard.absence.AddAbsence.AddAbsenceActivity
import net.mobitouch.intranet.ui.dashboard.home.HomeFragment
import net.mobitouch.intranet.ui.dashboard.password.PasswordFragment
import net.mobitouch.intranet.ui.dashboard.password.PasswordPresenter
import net.mobitouch.intranet.ui.dashboard.profile.profile_fragment.ProfileFragment
import net.mobitouch.intranet.ui.dashboard.profile.profile_fragment.ProfilePresenter
import net.mobitouch.intranet.ui.dashboard.profile.profile_home.ProfileHomeFragment
import net.mobitouch.intranet.ui.login.LoginActivity
import javax.inject.Inject

class DashboardActivity : DaggerAppCompatActivity(), DashboardView, BottomNavigationDrawerFragment.ClickListener {

    @Inject
    lateinit var presenter: DashboardPresenter

    @Inject
    lateinit var presenterProfile: ProfilePresenter

    @Inject
    lateinit var presenterPassword: PasswordPresenter

    var fragmentId = R.id.home

    override fun startLoginActivity() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    override fun showImage(link: String) {
        Glide.with(this).load("http://dev.intra.mobitouch.net$link").apply(RequestOptions.circleCropTransform()).into(userPhoto)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(bottom_app_bar)
        AndroidInjection.inject(this)
        presenter.attach(this)
        presenter.getImage()

        userPhoto.setOnClickListener {
            supportFragmentManager.beginTransaction().replace(R.id.fullscreen_content, ProfileFragment()).commit()
        }

        fabListener()
        supportFragmentManager.beginTransaction().replace(
            R.id.fullscreen_content,
            HomeFragment()
        ).commit()
    }



    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                val bottomNavDrawerFragment = BottomNavigationDrawerFragment()
                bottomNavDrawerFragment.setClickListener(this)
                bottomNavDrawerFragment.show(supportFragmentManager, bottomNavDrawerFragment.tag)
            }
        }
        return true
    }

    override fun onItemClick(fragmentId: Int) {
        this.fragmentId = fragmentId
        when (fragmentId) {
            R.id.profile ->
                fab.setImageResource(R.drawable.ic_save_black_24dp)

            else -> fab.setImageResource(R.drawable.ic_add_black_24dp)
        }
        //   fab.setImageResource(fragmentId)
    }

    fun fabListener() {
        fab.setOnClickListener {
            when (fragmentId) {
                R.id.profile -> {

                    (supportFragmentManager.findFragmentByTag("profile") as ProfileHomeFragment?)?.let {
                          it.save()
                    }


                }
                else -> {
                    val intent = Intent (this, AddAbsenceActivity::class.java).apply {  }
                    startActivity(intent)
                }
            }


        }
    }

}
