package net.mobitouch.intranet.ui.dashboard.card

import net.mobitouch.intranet.model.Card
import net.mobitouch.intranet.model.DataResponse
import net.mobitouch.intranet.model.Skill
import net.mobitouch.intranet.repository.local.TokenDatabse
import net.mobitouch.intranet.repository.remote.Api
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class CardPresenter @Inject constructor(val db: TokenDatabse, val retrofit: Retrofit) {
    val api = retrofit.create(Api::class.java)

    lateinit var cardView: CardView
    fun attach(cardView: CardView) {
        this.cardView = cardView
    }

    fun userCard() {
        val token = db.tokenDao().getAll().last().token
        api.getCard(token)
            .enqueue(object : retrofit2.Callback<DataResponse<Card>> {
                override fun onResponse(call: Call<DataResponse<Card>>, response: Response<DataResponse<Card>>) {
                    response.body()?.data?.let {
                        val experience = it.experience
                        val userLanguages = it.userLanguages
                        val skills = it.skills
                        val education = it.education
                        val certificates = it.certificates
                        val projects = it.projects

                        cardView.showSkills(skills.joinToString { skill -> "${skill.name}, ${skill.level} " })
                        cardView.showUserLanguages(userLanguages.joinToString { languages ->
                            "${languages.language}, " +
                                    "${languages.proficiency}, ${languages.speakigProficiency} "
                        })
                        cardView.showProjects(projects.joinToString { projects ->
                            "${projects.dateFrom}" +
                                    "${projects.dateTo},${projects.name},${projects.about}, ${projects.period}" +
                                    "${projects.team},${projects.technologies},${projects.sector}"
                        })
                        cardView.showExperience(experience.joinToString { workexperience ->
                            "${workexperience.period}," +
                                    " ${workexperience.name}, ${workexperience.position}"
                        })
                        cardView.showEducation(education.joinToString { education ->
                            "${education.diploma}, ${education.filedOfStudy}," +
                                    "${education.specialization}, ${education.title}"
                        })
                        cardView.showCertificates(certificates.joinToString { certificates -> "${certificates.name}" })
                    }

                }

                override fun onFailure(call: Call<DataResponse<Card>>, t: Throwable) {
                }
            })


    }

    fun skillsToString(skills: List<Skill>): String {
        var result: String = ""
        for (skill in skills) {
            val name = skill.name
            result += name

        }
        return result
    }
}