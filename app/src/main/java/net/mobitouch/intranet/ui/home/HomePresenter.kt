package net.mobitouch.intranet.ui.home

import net.mobitouch.intranet.model.DataResponse
import net.mobitouch.intranet.model.Statistics
import net.mobitouch.intranet.repository.local.TokenDatabse
import retrofit2.Call
import retrofit2.Response
import net.mobitouch.intranet.repository.remote.RetrofitClientInstance

class HomePresenter(val homeView: HomeView, val db: TokenDatabse) {
    val api = RetrofitClientInstance().get()
    val token = db.tokenDao().getAll().last().token

   fun dashboard(){
       api.statistics(token)
           .enqueue(object : retrofit2.Callback<DataResponse<Statistics>> {
               override fun onResponse(call: Call<DataResponse<Statistics>>, response: Response<DataResponse<Statistics>>) {
                   val all: String = response.body()?.data?.all.toString()
                   val yearly: String = response.body()?.data?.all.toString()
                   val monthly: String = response.body()?.data?.monthly.toString()
                   val weekly: String = response.body()?.data?.weekly.toString()

                   homeView.showAll(all)
                   homeView.showMonthly(monthly)
                   homeView.showYearly(yearly)
               }

               override fun onFailure(call: Call<DataResponse<Statistics>>, t: Throwable) {

               }

           })
   }


}