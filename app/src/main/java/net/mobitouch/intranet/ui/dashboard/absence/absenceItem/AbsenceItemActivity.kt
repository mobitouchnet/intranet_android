package net.mobitouch.intranet.ui.dashboard.absence.absenceItem

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_absence_item.*
import net.mobitouch.intranet.R
import net.mobitouch.intranet.model.AbsenceList
import javax.inject.Inject

class AbsenceItemActivity : AppCompatActivity(), AbsenceItemView {
    @SuppressLint("SetTextI18n")
    override fun showAbsence(absenceList: AbsenceList) {
        dateFrom.text = "Od: ${absenceList.date_from}"
        dateTo.text = "Do: ${absenceList.date_to}"
        days.text = "Dni: ${absenceList.days}"
        when (absenceList.status){
            "to_accept" -> status.text = "Status: Do akceptacji"
            "accepted" -> status.text = "Status: Zaakceptowany"
            "rejected" -> status.text = "Status: Odrzucony"
        }
        when (absenceList.reason){
            "ABSENCE_REASON_SICKNESS" -> reason.text = "Typ nieobecności: Choroba"
            "ABSENCE_REASON_DELEGATION" -> reason.text = "Typ nieobecności: Delegacja"
            "ABSENCE_REASON_CHILDCARE" -> reason.text = "Typ nieobecności: Opieka nad dzieckiem"
            "ABSENCE_REASON_REMOTE_WORK" -> reason.text = "Typ nieobecności: Praca zdalna"
            "ABSENCE_REASON_UNPAID_LEAVE" -> reason.text = "Typ nieobecności: Urlop bezpłatny"
            "ABSENCE_REASON_MATERNITY_LEAVE" -> reason.text = "Typ nieobecności: Urlop macierzyński"
            "ABSENCE_REASON_ON_DEMAND" -> reason.text = "Typ nieobecności: Urlop na żądanie"
            "ABSENCE_REASON_PATERNITY_LEAVE" -> reason.text = "Typ nieobecności: Urlop ojcowski"
            "ABSENCE_REASON_OCCASIONAL_LEAVE" -> reason.text = "Typ nieobecności: Urlop okolicznościowy"
            "ABSENCE_REASON_PARENTAL_LEAVE" -> reason.text = "Typ nieobecności: Urlop rodzicielski"
            "ABSENCE_REASON_VACATION_LEAVE" -> reason.text = "Typ nieobecności: Urlop wypoczynkowy"


        }


    }

    @Inject
    lateinit var absenceItemPresenter: AbsenceItemPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_absence_item)
        AndroidInjection.inject(this)
        absenceItemPresenter.attach(this)
        val absence : AbsenceList = intent.getParcelableExtra("absence")
        showAbsence(absence)
    }
}
