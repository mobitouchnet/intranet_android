package net.mobitouch.intranet.di

import javax.inject.Scope

@Scope
@Retention
annotation class FragmentScope