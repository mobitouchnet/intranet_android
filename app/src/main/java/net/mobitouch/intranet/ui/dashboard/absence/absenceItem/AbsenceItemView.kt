package net.mobitouch.intranet.ui.dashboard.absence.absenceItem

import net.mobitouch.intranet.model.AbsenceList

interface AbsenceItemView {
    fun showAbsence(it:AbsenceList)
}