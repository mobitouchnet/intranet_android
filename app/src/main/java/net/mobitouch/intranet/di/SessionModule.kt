package net.mobitouch.intranet.di

import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import net.mobitouch.intranet.App

@Module(subcomponents = [SessionComponent::class], includes = [SessionProviderModule::class])
internal abstract class SessionModule {
@Binds
internal abstract fun injector(component: SessionComponent):AndroidInjector<App>
}
