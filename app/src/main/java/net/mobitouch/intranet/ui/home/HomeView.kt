package net.mobitouch.intranet.ui.home

interface HomeView{
    fun showAll (textMessage:String)
    fun showYearly (textMessage: String)
    fun showMonthly (textMessage: String)
}