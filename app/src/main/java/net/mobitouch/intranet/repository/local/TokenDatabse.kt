package net.mobitouch.intranet.repository.local

import androidx.room.Database
import androidx.room.RoomDatabase
import net.mobitouch.intranet.repository.local.dao.TokenDao
import net.mobitouch.intranet.model.TokenData

@Database(entities = arrayOf(
    TokenData::class), version = 1, exportSchema = false)
abstract class TokenDatabse : RoomDatabase() {
    abstract fun tokenDao(): TokenDao
}