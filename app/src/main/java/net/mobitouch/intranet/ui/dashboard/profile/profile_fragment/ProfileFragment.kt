package net.mobitouch.intranet.ui.dashboard.profile.profile_fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_profile.*
import net.mobitouch.intranet.R
import net.mobitouch.intranet.model.ProfileSave
import javax.inject.Inject

class ProfileFragment @Inject constructor(): Fragment(), ProfileView {
    override fun showName(textMessage: String) {
        editName.setText(textMessage)
    }

    override fun showLastName(textMessage: String) {
        editLastName.setText(textMessage)
    }

    override fun showPosition(textMessage: String) {
        editPosition.setText(textMessage)
    }

    override fun showWorkingSince(textMessage: String) {
        editWorkingSince.setText(textMessage)
    }

    override fun showBirthdayDate(textMessage: String) {
        editDateBirthday.setText(textMessage)
    }

    override fun showInterests(textMessage: String) {
        editInterests.setText(textMessage)
    }

    override fun showPhoneNumber(textMessage: String) {
        editPhone.setText(textMessage)
    }

    override fun showRole(textMessage: String) {
        editRole.setText(textMessage)
    }

    override fun showPhoto(textMessage: String) {
        editPhoto.setText(textMessage)
    }

    @Inject
    lateinit var presenter: ProfilePresenter

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        presenter.attach(this)
        presenter.userProfile()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    fun save (){
        presenter.save(
            ProfileSave(editName.text.toString(),editLastName.text.toString(),editInterests.text.toString(),editPosition.text.toString(),
                editWorkingSince.text.toString(),editDateBirthday.text.toString(),editRole.text.toString(),editPhone.text.toString())
        )

    }

}

