package net.mobitouch.intranet.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.room.Room
import net.mobitouch.intranet.repository.local.TokenDatabse
import kotlinx.android.synthetic.main.fragment_home.*

import net.mobitouch.intranet.R


class HomeFragment : Fragment(), HomeView {
    override fun showYearly(textMessage: String) {
        textNumberYearProposal.text = textMessage
    }

    override fun showMonthly(textMessage: String) {
        textNumberMonthProposal.text = textMessage
    }

    override fun showAll(textMessage: String) {
        textNumberOfProposal.text = textMessage
    }

    lateinit var presenter: HomePresenter



    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_home, parent, false)

        val db = Room.databaseBuilder(activity!!.applicationContext, TokenDatabse::class.java, "token.db")
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()

        presenter = HomePresenter(this, db)

        presenter.dashboard()

        return view

    }


}