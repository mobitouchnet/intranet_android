package net.mobitouch.intranet.model

data class Skill(
    val id: String,
    val name: String,
    val category: String,
    val level: String
)