package net.mobitouch.intranet.di

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import net.mobitouch.intranet.App
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        SessionModule::class,
        DomainToolsModule::class,
        ActivitiesModule::class,
        AndroidSupportInjectionModule::class
    ]
)


interface AppComponent : AndroidInjector<App> {
    fun androidInjector(): AndroidInjector<App>

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>() {
        abstract override fun build(): AppComponent
    }
}