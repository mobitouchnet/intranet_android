package net.mobitouch.intranet.ui.dashboard.card

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_card.*
import net.mobitouch.intranet.R
import net.mobitouch.intranet.model.Skill
import javax.inject.Inject

class CardFragment @Inject constructor(): Fragment(), CardView {

    override fun showCertificates(textMessage: String) {
        editCertificates.setText(textMessage)
    }

    override fun showEducation(textMessage: String) {
        editEducation.setText(textMessage)
    }

    override fun showExperience(textMessage: String) {
        editExperience.setText(textMessage)
    }

    override fun showProjects(textMessage: String) {
        editProjects.setText(textMessage)
    }

    override fun showSkills(textMessage: String) {
        editSkills.setText(textMessage)
    }

    override fun showUserLanguages(textMessage: String) {
        editLanguages.setText(textMessage)
    }

    @Inject
    lateinit var presenter: CardPresenter

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_card, container, false)
        presenter.attach(this)
        presenter.userCard()
        return view
    }
}