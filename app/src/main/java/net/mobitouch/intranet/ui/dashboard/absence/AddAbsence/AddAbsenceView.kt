package net.mobitouch.intranet.ui.dashboard.absence.AddAbsence

interface AddAbsenceView{
    fun showErrorMessage(errorMessage: String)
    fun showCorrectMessage(textMessage: String)
}