package net.mobitouch.intranet.ui.dashboard.users

import net.mobitouch.intranet.model.User

interface UsersView{
    fun showAllUsers(usersList: List<User>)
}