package net.mobitouch.intranet.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tokenData")
data class TokenData(
    @PrimaryKey
    var token: String
)

