package net.mobitouch.intranet.ui.splash

interface SplashView {
    fun startLoginActivity()
    fun startDashboardAtivity()
}