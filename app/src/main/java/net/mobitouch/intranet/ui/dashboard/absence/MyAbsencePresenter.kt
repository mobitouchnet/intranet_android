package net.mobitouch.intranet.ui.dashboard.absence

import net.mobitouch.intranet.model.AbsenceList
import net.mobitouch.intranet.model.DataResponse
import net.mobitouch.intranet.model.Requests
import net.mobitouch.intranet.repository.local.TokenDatabse
import net.mobitouch.intranet.repository.remote.Api
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class MyAbsencePresenter @Inject constructor(val db: TokenDatabse, val retrofit: Retrofit) {
    val api = retrofit.create(Api::class.java)

    lateinit var myAbsenceView: MyAbsenceView
    fun attach(myAbsenceView: MyAbsenceView) {
        this.myAbsenceView = myAbsenceView
    }

    fun getAbsence() {
        val token = db.tokenDao().getAll().last().token
        api.getAbsence(token)
            .enqueue(object : retrofit2.Callback<DataResponse<List<Requests>>> {
                override fun onResponse(
                    call: Call<DataResponse<List<Requests>>>,
                    response: Response<DataResponse<List<Requests>>>
                ) {
                    val absence = response.body()?.data
                    if (absence != null) {
                        var absenceList = mutableListOf<AbsenceList>()

                        for (item in absence) {
                            absenceList.addAll(item.requests)
                        }
                        myAbsenceView.showAbsence(absenceList)

                    }
                }

                override fun onFailure(call: Call<DataResponse<List<Requests>>>, t: Throwable) {

                }
            })
    }

}