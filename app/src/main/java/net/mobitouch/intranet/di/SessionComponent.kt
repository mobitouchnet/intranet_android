package net.mobitouch.intranet.di

import dagger.Subcomponent
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import net.mobitouch.intranet.App


@SessionScope
@Subcomponent(
    modules = [
        ActivitiesModule::class,
        AndroidSupportInjectionModule::class
    ])
interface SessionComponent : AndroidInjector<App> {
    @Subcomponent.Builder
    abstract class Builder {
        abstract fun build(): SessionComponent
    }
}