package net.mobitouch.intranet.ui.dashboard.home

import net.mobitouch.intranet.model.DataResponse
import net.mobitouch.intranet.model.Statistics
import net.mobitouch.intranet.repository.local.TokenDatabse
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import net.mobitouch.intranet.repository.remote.Api
import javax.inject.Inject

class HomePresenter @Inject constructor (val db: TokenDatabse, val retrofit: Retrofit) {
    val api = retrofit.create(Api::class.java)
    val token = db.tokenDao().getAll().last().token


    lateinit var homeView:HomeView
    fun attach(homeView: HomeView) {
        this.homeView = homeView
    }


   fun dashboard(){
       api.statistics(token)
           .enqueue(object : retrofit2.Callback<DataResponse<Statistics>> {
               override fun onResponse(call: Call<DataResponse<Statistics>>, response: Response<DataResponse<Statistics>>) {
                   response.body()?.data?.let {
                       val all: String = it.all
                       val yearly: String = it.yearly
                       val monthly: String = it.monthly
                       val weekly: String = it.weekly

                       homeView.showAll(all)
                       homeView.showMonthly(monthly)
                       homeView.showYearly(yearly)
                   }
               }
               override fun onFailure(call: Call<DataResponse<Statistics>>, t: Throwable) {

               }

           })
   }


}