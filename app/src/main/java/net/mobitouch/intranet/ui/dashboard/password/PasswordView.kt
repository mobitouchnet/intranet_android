package net.mobitouch.intranet.ui.dashboard.password

interface PasswordView {
    fun showErrorMessage(errorMessage: String)
    fun showCorrectMessage(textMessage: String)
}