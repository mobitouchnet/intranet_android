package net.mobitouch.intranet.model

import com.google.gson.annotations.SerializedName

data class Card (
    val experience: List<Workexperience>,
    @SerializedName(value = "user_languages")
    val userLanguages: List<Languages>,
    val skills: List<Skill>,
    val education: List<Education>,
    val certificates: List<Certificates>,
    val projects: List<Projects>
)