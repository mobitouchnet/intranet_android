package net.mobitouch.intranet.ui.dashboard.absence

import net.mobitouch.intranet.model.AbsenceList
import net.mobitouch.intranet.model.Requests

interface MyAbsenceView {
    fun showAbsence(absence:List<AbsenceList>)
}