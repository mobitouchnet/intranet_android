package net.mobitouch.intranet.ui.dashboard.profile.profile_fragment

interface ProfileView {
    fun showName(textMessage: String)
    fun showLastName(textMessage: String)
    fun showPosition(textMessage: String)
    fun showWorkingSince(textMessage: String)
    fun showBirthdayDate(textMessage: String)
    fun showInterests(textMessage: String)
    fun showPhoneNumber(textMessage: String)
    fun showPhoto(textMessage: String)
    fun showRole(textMessage: String)
}