package net.mobitouch.intranet.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import net.mobitouch.intranet.ui.dashboard.DashboardActivity
import net.mobitouch.intranet.ui.dashboard.DashboardActivityModule
import net.mobitouch.intranet.ui.dashboard.absence.AddAbsence.AddAbsenceActivity
import net.mobitouch.intranet.ui.dashboard.absence.absenceItem.AbsenceItemActivity
import net.mobitouch.intranet.ui.dashboard.users.userItem.UserItemActivity
import net.mobitouch.intranet.ui.login.LoginActivity
import net.mobitouch.intranet.ui.splash.SplashScreenActivity

@Module
internal abstract class ActivitiesModule {
    @ActivityScope
    @ContributesAndroidInjector
    internal abstract fun splashActivity(): SplashScreenActivity

    @ActivityScope
    @ContributesAndroidInjector
    internal abstract fun loginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector
    internal abstract fun userItemActivity(): UserItemActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [DashboardActivityModule::class])
    internal abstract fun dashboardActivity(): DashboardActivity

    @ActivityScope
    @ContributesAndroidInjector
    internal abstract fun addAbsenceActivity(): AddAbsenceActivity

    @ActivityScope
    @ContributesAndroidInjector
    internal abstract fun absenceItemActivity(): AbsenceItemActivity
}
