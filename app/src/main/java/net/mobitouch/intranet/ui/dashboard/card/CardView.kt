package net.mobitouch.intranet.ui.dashboard.card

import net.mobitouch.intranet.model.Skill

interface CardView {
    fun showExperience(textMessage: String)
    fun showUserLanguages(textMessage: String)
    fun showSkills(textMessage: String)
    fun showEducation(textMessage: String)
    fun showCertificates(textMessage: String)
    fun showProjects(textMessage: String)
}