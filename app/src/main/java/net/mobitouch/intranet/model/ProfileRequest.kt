package net.mobitouch.intranet.model

data class ProfileRequest(
        val name:String,
        val lastName:String,
        val interests:String,
        val position:String,
        val workingSince:String,
        val birthdayDate:String,
        val companyRoles:String,
        val phoneNumber:String
)