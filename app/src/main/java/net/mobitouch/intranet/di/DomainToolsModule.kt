package net.mobitouch.intranet.di

import androidx.room.Room
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import net.mobitouch.intranet.App
import net.mobitouch.intranet.repository.local.TokenDatabse
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class DomainToolsModule{

    @Provides
    @Singleton
    internal fun provideRetrofit(gson: Gson, okhttp:OkHttpClient):Retrofit{
        return Retrofit.Builder().client(okhttp)
            .baseUrl("http://dev.intra.mobitouch.net/api/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Provides
    @Singleton
    internal fun provideGson():Gson{
        return GsonBuilder().create()
    }

    @Provides
    @Singleton
    internal fun provideOkHttpClient():OkHttpClient{
        return OkHttpClient.Builder().addNetworkInterceptor(StethoInterceptor()).build()
    }

    @Provides
    @Singleton
    internal fun provideDatabase(app: App): TokenDatabse {
        return Room.databaseBuilder(app.applicationContext, TokenDatabse::class.java, "token.db")
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
    }



   // fun get() = retrofit().create(Api::class.java)
}