package net.mobitouch.intranet.ui.dashboard.users

import com.bumptech.glide.annotation.GlideModule
import net.mobitouch.intranet.model.DataResponse
import net.mobitouch.intranet.model.User
import net.mobitouch.intranet.repository.local.TokenDatabse
import net.mobitouch.intranet.repository.remote.Api
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class UsersPresenter @Inject constructor(val db: TokenDatabse, val retrofit: Retrofit) {
    val api = retrofit.create(Api::class.java)


    lateinit var usersView: UsersView
    fun attach(usersView: UsersView) {
        this.usersView = usersView
    }

    fun usersList() {
        val token = db.tokenDao().getAll().last().token
        api.getUsersList(token)
            .enqueue(object : retrofit2.Callback<DataResponse<List<User>>> {
                override fun onResponse(call: Call<DataResponse<List<User>>>, response: Response<DataResponse<List<User>>>) {
                    val users = response.body()?.data
                    if (users!=null){
                        usersView.showAllUsers(users)
                    }
                }
                override fun onFailure(call: Call<DataResponse<List<User>>>, t: Throwable) {

                }
            })
    }


}