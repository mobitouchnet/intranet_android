package net.mobitouch.intranet.model

data class Projects(
    val name : String,
    val dateFrom: String,
    val dateTo: String,
    val team: String,
    val technologies: String,
    val about: String,
    val period:String,
    val sector:String
)