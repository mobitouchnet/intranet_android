package net.mobitouch.intranet.di

import dagger.Module
import dagger.Provides

@Module (subcomponents = [SessionComponent::class])
internal class SessionProviderModule{
    @Provides
    internal fun providesSessionComponent(builder: SessionComponent.Builder):SessionComponent{
        return builder.build()
    }
  }

