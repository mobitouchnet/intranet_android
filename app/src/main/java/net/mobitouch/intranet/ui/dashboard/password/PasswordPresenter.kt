package net.mobitouch.intranet.ui.dashboard.password

import net.mobitouch.intranet.model.DataResponse
import net.mobitouch.intranet.model.TokenData
import net.mobitouch.intranet.repository.local.TokenDatabse
import net.mobitouch.intranet.repository.remote.Api
import net.mobitouch.intranet.ui.dashboard.DashboardActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class PasswordPresenter @Inject constructor(val db: TokenDatabse, val retrofit: Retrofit) {

    val api = retrofit.create(Api::class.java)
    lateinit var passwordView: PasswordView
    fun attach(passwordView: PasswordView) {
        this.passwordView = passwordView
    }

    fun changePassword(oldPassword: String, newPassword: String) {
        val token = db.tokenDao().getAll().last().token
        api.changePassword(token, oldPassword, newPassword)
            .enqueue(object : Callback<DataResponse<Unit>> {
                override fun onResponse(call: Call<DataResponse<Unit>>, response: Response<DataResponse<Unit>>) {
                    if (response.code() == 200) {
//                        passwordView.showCorrectMessage("Zapisano")
                    } else {
//                        passwordView.showErrorMessage("Error")
                    }
                }

                override fun onFailure(call: Call<DataResponse<Unit>>, t: Throwable) {
//                    passwordView.showErrorMessage("Error2")
                }
            })

    }

}
