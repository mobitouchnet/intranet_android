package net.mobitouch.intranet.model

data class AuthRequest(
    val email: String,
    val password: String
)