package net.mobitouch.intranet.ui.dashboard.profile.profile_home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TabHost
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_home_profile.*
import kotlinx.android.synthetic.main.fragment_home_profile.view.*
import net.mobitouch.intranet.R
import net.mobitouch.intranet.ui.dashboard.DashboardActivity
import net.mobitouch.intranet.ui.dashboard.card.CardFragment
import net.mobitouch.intranet.ui.dashboard.password.PasswordFragment
import net.mobitouch.intranet.ui.dashboard.profile.profile_fragment.ProfileFragment


class ProfileFragmentHome : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home_profile, parent, false)
        val viewPager = view.viewPager as ViewPager
        setupViewPager(viewPager)
        viewPager.offscreenPageLimit = 2
        val tabs = view.tabLayout as TabLayout
        tabs.setupWithViewPager(viewPager)
        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {

            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                when (tabs.tabCount) {
                    0 -> showProfile()
                    1 -> showCard()
                    2 -> showPassword()
                }
            }


        })

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    fun showProfile() {
        fragmentManager!!.beginTransaction().replace(R.id.viewPager, ProfileFragment()).commit()
    }

    fun showCard() {
        fragmentManager!!.beginTransaction().replace(R.id.viewPager, CardFragment()).commit()
    }

    fun showPassword() {
        fragmentManager!!.beginTransaction().replace(R.id.viewPager, PasswordFragment()).commit()
    }




    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = TabAdapter(getChildFragmentManager())
        adapter.addFragment(ProfileFragment(), "Profil")
        adapter.addFragment(CardFragment(), "Wizytówka")
        adapter.addFragment(PasswordFragment(), "Hasło")
        viewPager.setAdapter(adapter)
    }

}

