package net.mobitouch.intranet.ui.dashboard.users.userItem

import net.mobitouch.intranet.model.DataResponse
import net.mobitouch.intranet.model.User
import net.mobitouch.intranet.repository.local.TokenDatabse
import net.mobitouch.intranet.repository.remote.Api
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.http.GET
import javax.inject.Inject

class UserItemPresenter @Inject constructor(val db: TokenDatabse, val retrofit: Retrofit) {
    val api = retrofit.create(Api::class.java)
    lateinit var userItemView: UserItemView
    fun attach(userItemView: UserItemView) {
        this.userItemView = userItemView
    }

    fun getUserItem() {
        val token = db.tokenDao().getAll().last().token
        api.getUserById(token)
            .enqueue(object : retrofit2.Callback<DataResponse<User>> {
                override fun onResponse(call: Call<DataResponse<User>>, response: Response<DataResponse<User>>) {
                    response.body()?.data?.let {
                        userItemView.showUser(it)
                    }

                }

                override fun onFailure(call: Call<DataResponse<User>>, t: Throwable) {

                }
            })
    }
    fun getUserbyId (id: String){
        val token = db.tokenDao().getAll().last().token
        api.getUserItem(token, id)
            .enqueue(object : retrofit2.Callback<DataResponse<User>> {
                override fun onResponse(call: Call<DataResponse<User>>, response: Response<DataResponse<User>>) {
                    response.body()?.data?.let {
                userItemView.showUser(it)

                    }

                }

                override fun onFailure(call: Call<DataResponse<User>>, t: Throwable) {

                }
            })
    }

}