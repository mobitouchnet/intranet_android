package net.mobitouch.intranet.model

data class Education(
    val diploma: String,
    val filedOfStudy: String,
    val specialization: String,
    val title: String
)