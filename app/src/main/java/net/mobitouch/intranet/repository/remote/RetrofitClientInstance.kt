package net.mobitouch.intranet.repository.remote

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class RetrofitClientInstance {

    val gson = GsonBuilder()
        .create()

    fun retrofit(): Retrofit {
        val okhttp = OkHttpClient.Builder().addNetworkInterceptor(StethoInterceptor()).build()
        return Retrofit.Builder().client(okhttp)
            .baseUrl("http://dev.intra.mobitouch.net/api/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    fun get() = retrofit().create(Api::class.java)
}