package net.mobitouch.intranet.model

data class ProfileSave(
    val name: String,
    val lastName: String,
    val interests: String,
    val position: String,
    val workingSince: String,
    val birthdayDate: String,
    val companyRoles: String,
    val phoneNumber: String
) {
    fun gethashMap(): HashMap<String, Any> =
        hashMapOf(
            Pair("name", name),
            Pair("lastName", lastName),
            Pair("interests", interests),
            Pair("position", position),
            Pair("workingSince", workingSince),
            Pair("birthdayDate", birthdayDate),
            Pair("companyRoles", companyRoles),
            Pair("phoneNumber", phoneNumber)
        )
}