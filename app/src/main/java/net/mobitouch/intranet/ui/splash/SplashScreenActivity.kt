package net.mobitouch.intranet.ui.splash

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import dagger.android.AndroidInjection
import net.mobitouch.intranet.R
import net.mobitouch.intranet.ui.dashboard.DashboardActivity
import net.mobitouch.intranet.ui.login.LoginActivity
import net.mobitouch.intranet.ui.login.LoginPresenter
import net.mobitouch.intranet.ui.main.MainActivity
import javax.inject.Inject

class SplashScreenActivity : AppCompatActivity(), SplashView {

    override fun startLoginActivity() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    override fun startDashboardAtivity() {
        val intent = Intent(this,DashboardActivity::class.java)
        startActivity(intent)
    }


    @Inject
    lateinit var presenter: SplashPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        presenter.attach(this)
        setContentView(R.layout.activity_splash)
        presenter.isLoggedIn()
    }

}

