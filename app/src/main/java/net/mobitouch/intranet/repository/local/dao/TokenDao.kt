package net.mobitouch.intranet.repository.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import net.mobitouch.intranet.model.TokenData

@Dao
interface TokenDao {
    @Query("SELECT * from tokenData")
    fun getAll(): List<TokenData>

    @Insert(onConflict = REPLACE)
    fun insert(tokenData: TokenData)

    @Query("DELETE from tokenData")
    fun deleteAll()

}