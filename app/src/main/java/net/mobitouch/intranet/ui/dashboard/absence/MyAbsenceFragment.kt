package net.mobitouch.intranet.ui.dashboard.absence

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SimpleAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_my_absence.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*

import net.mobitouch.intranet.R
import net.mobitouch.intranet.model.AbsenceList
import net.mobitouch.intranet.model.Requests
import net.mobitouch.intranet.ui.dashboard.absence.absenceItem.AbsenceItemActivity
import javax.inject.Inject


class MyAbsenceFragment : Fragment(), MyAbsenceView {

    override fun showAbsence(absence: List<AbsenceList>) {
        adapter.setAbsence(absence.toMutableList())
    }

    @Inject
    lateinit var presenter: MyAbsencePresenter

    @Inject
    lateinit var adapter: AbsenceAdapter


    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_my_absence, parent, false)
        AndroidSupportInjection.inject(this)
        presenter.attach(this)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        absenceRecyclerView.layoutManager = LinearLayoutManager(absenceRecyclerView.context)
        adapter.setOnClickListener {absence->
            val intent = Intent(activity, AbsenceItemActivity::class.java)
            intent.putExtra("absence", absence)
            startActivity(intent)
        }
        absenceRecyclerView.adapter = adapter

        presenter.getAbsence()
        val dividerItemDecoration = DividerItemDecoration(absenceRecyclerView.context, LinearLayoutManager.VERTICAL)
        absenceRecyclerView.addItemDecoration(dividerItemDecoration)
        val swipeHandler = object : SwipeToDeleteCallback(activity!!.baseContext) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                adapter.removeAt(viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(absenceRecyclerView)

    }


}
