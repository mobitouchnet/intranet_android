package net.mobitouch.intranet.ui.dashboard.users

import android.app.Application
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.user_list_item.view.*
import net.mobitouch.intranet.R
import net.mobitouch.intranet.model.User
import javax.inject.Inject


class UsersListAdapter @Inject constructor() : RecyclerView.Adapter<UsersListAdapter.ViewHolder>() {

    private var userList: List<User> = emptyList()
    private var onClick: (User) -> Unit = {}

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindUser(userList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.user_list_item, parent, false)
        return ViewHolder(v, onClick)
    }


    fun setUserList(userList: MutableList<User>) {
        this.userList = userList
        notifyDataSetChanged()
    }

    fun setOnClickListener(onClick: (User) -> Unit) {
        this.onClick = onClick
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    class ViewHolder(itemView: View, val onClick: (User) -> Unit) : RecyclerView.ViewHolder(itemView) {
        lateinit var retrofitUsers: User

        fun bindUser(user: User) {
            retrofitUsers = user
            itemView.textUserName.text = retrofitUsers.name
            itemView.textUserLastName.text = retrofitUsers.lastName
//        holder.progressBar.visibility = View
//        holder.progressBar.progress = 1500
            Glide.with(itemView.photo.context).load("http://dev.intra.mobitouch.net" + retrofitUsers.photo)
                .apply(RequestOptions.circleCropTransform()).centerCrop()
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        itemView.progressBar2.visibility = View.INVISIBLE
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        itemView.progressBar2.visibility = View.INVISIBLE
                        return false
                    }
                })
                .placeholder(R.drawable.placeholder).apply(RequestOptions.circleCropTransform())
                .into(itemView.photo)
            itemView.root.setOnClickListener {
                onClick(retrofitUsers)
            }
        }
    }


}