package net.mobitouch.intranet.ui.dashboard

import android.util.Log
import net.mobitouch.intranet.model.DataResponse
import net.mobitouch.intranet.model.User
import net.mobitouch.intranet.repository.local.TokenDatabse
import net.mobitouch.intranet.repository.remote.Api
import net.mobitouch.intranet.ui.dashboard.profile.profile_fragment.ProfileView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class DashboardPresenter @Inject constructor(val db: TokenDatabse, val retrofit: Retrofit) {
    lateinit var dashboardView: DashboardView
    fun attach(dashboardView: DashboardView) {
        this.dashboardView = dashboardView
    }

    val api = retrofit.create(Api::class.java)
    fun logout() {
        db.tokenDao().deleteAll()
        dashboardView.startLoginActivity()
    }

    fun getImage() {
        val token = db.tokenDao().getAll().last().token
        api.getUserById(token)
            .enqueue(object : Callback<DataResponse<User>> {
                override fun onResponse(call: Call<DataResponse<User>>, response: Response<DataResponse<User>>) {
                    val photo = response.body()?.data?.photo.toString()
                    dashboardView.showImage(photo)
                }

                override fun onFailure(call: Call<DataResponse<User>>, t: Throwable) {
                    Log.e("error", "error")
                }
            })
    }
}