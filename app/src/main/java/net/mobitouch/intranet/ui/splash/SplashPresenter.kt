package net.mobitouch.intranet.ui.splash

import net.mobitouch.intranet.repository.local.TokenDatabse
import javax.inject.Inject

class SplashPresenter @Inject constructor(val db: TokenDatabse) {

    lateinit var splashView: SplashView
    fun attach(splashView: SplashView) {
        this.splashView = splashView
    }

    fun isLoggedIn () {
         object : Thread() {
            override fun run() {
                try {
                    Thread.sleep(3000)
                    val listToken = db.tokenDao().getAll()

                    if (listToken.isNotEmpty()) {
                        splashView.startDashboardAtivity()
                    } else {
                        splashView.startLoginActivity()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }.start()
    }
}