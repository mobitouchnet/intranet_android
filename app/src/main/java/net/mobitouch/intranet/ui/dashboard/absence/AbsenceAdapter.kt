package net.mobitouch.intranet.ui.dashboard.absence

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.absence_item.view.*
import net.mobitouch.intranet.R
import net.mobitouch.intranet.model.AbsenceList
import javax.inject.Inject

class AbsenceAdapter @Inject constructor() : RecyclerView.Adapter<AbsenceAdapter.ViewHolder>() {
    private var absenceList: MutableList<AbsenceList> = mutableListOf()
    private var onClick: (AbsenceList) -> Unit = {}
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindAbsence(absenceList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.absence_item, parent, false)
        return ViewHolder(view, onClick)
    }

    fun setAbsence(absenceList: List<AbsenceList>) {
        this.absenceList = absenceList.toMutableList()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return absenceList.size
    }


    class ViewHolder(itemView: View, val onClick: (AbsenceList) ->Unit) : RecyclerView.ViewHolder(itemView) {
        lateinit var retrofitAbsenceList: AbsenceList
        val dateFrom = itemView.dateFrom
        val dateTo = itemView.date_to
        val days = itemView.days
        val status = itemView.status

        @SuppressLint("SetTextI18n")
        fun bindAbsence(absenceList: AbsenceList){
            retrofitAbsenceList = absenceList
            itemView.dateFrom.text = "Od " + retrofitAbsenceList.date_from
            itemView.date_to.text = "Do " + retrofitAbsenceList.date_to
            itemView.days.text = retrofitAbsenceList.days.toString() + " dni"
            when (retrofitAbsenceList.status) {
                "to_accept" -> itemView.status.text = "Do akceptacji"
                "rejected" -> itemView.status.text = "Odrzucony"
                "accepted" -> itemView.status.text = "Zaakceptowany"
            }
            itemView.absence_root.setOnClickListener {
                onClick(retrofitAbsenceList)
            }
        }
    }

    fun removeAt(position: Int) {
        absenceList.removeAt(position)
        notifyItemRemoved(position)
    }

    fun setOnClickListener(onClick: (AbsenceList) -> Unit){
        this.onClick = onClick
    }
}
