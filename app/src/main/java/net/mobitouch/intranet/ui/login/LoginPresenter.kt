package net.mobitouch.intranet.ui.login

import android.content.SharedPreferences
import android.util.Log
import android.util.Patterns
import net.mobitouch.intranet.R
import net.mobitouch.intranet.model.*
import net.mobitouch.intranet.repository.local.TokenDatabse
import net.mobitouch.intranet.repository.remote.Api
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

import javax.inject.Inject


class LoginPresenter @Inject constructor(val db: TokenDatabse, val retrofit: Retrofit) {

    
    lateinit var loginView: LoginView
    val api = retrofit.create(Api::class.java)

    fun attach(loginView: LoginView) {
        this.loginView = loginView
    }

    fun login(email: String, password: String) {

        if (checkLogin(email, password)) {

            api.authenticate(email, password)
                .enqueue(object : Callback<DataResponse<Token>> {
                    override fun onResponse(call: Call<DataResponse<Token>>, response: Response<DataResponse<Token>>) {
                        if (response.code() == 200) {
                            // implement database token save

                            val token = response.body()?.data?.token
                            if (token == null)
                                loginView.showErrorMessage("Serwer Error")
                            else {
                                db.tokenDao().insert(TokenData(token))
                                getUser()

                            }

                        } else if (response.code() == 400) {
                            loginView.showErrorMessage("Nie poprawne dane logowania")
                        }

                    }

                    override fun onFailure(call: Call<DataResponse<Token>>, t: Throwable) {

                    }


                })

        }
    }

    fun getUser() {
        val token = db.tokenDao().getAll().last().token
        api.getUser(token)
            .enqueue(object : Callback<DataResponse<User>> {
                override fun onResponse(call: Call<DataResponse<User>>, response: Response<DataResponse<User>>) {
                    if (response.code() == 200) {

                        loginView.startHomeActivity()

                    } else Log.e("Error", "400")
                }

                override fun onFailure(call: Call<DataResponse<User>>, t: Throwable) {

                }
            })
    }


    private fun checkEmail(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun checkPassword(password: String): Boolean {
        return (password.length > 3)
    }

    private fun checkLogin(email: String, password: String): Boolean {
        val correctEmail = checkEmail(email)
        Log.e("coreectEmail", correctEmail.toString())
        val correctPassword = checkPassword(password)
        Log.e("correctPassword", correctPassword.toString())
        if (correctEmail && correctPassword) return true
        else if (!correctEmail && !correctPassword) {
            loginView.showInccorectLoginMessage("nie poprawny login")
            loginView.showIncorectPasswordMessage("niepoprawne hasło")
        } else if (!correctEmail && correctPassword)
            loginView.showInccorectLoginMessage(R.string.app_name)
        else loginView.showIncorectPasswordMessage("niepoprawne hasło")
        return false
    }

}