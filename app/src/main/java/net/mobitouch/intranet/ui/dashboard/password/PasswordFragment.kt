package net.mobitouch.intranet.ui.dashboard.password

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_password.*
import net.mobitouch.intranet.R
import javax.inject.Inject

class PasswordFragment @Inject constructor() : Fragment(), PasswordView {


    override fun showErrorMessage(errorMessage: String) {
        Toast.makeText(this.context, errorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun showCorrectMessage(textMessage: String) {
        Toast.makeText(this.context, textMessage, Toast.LENGTH_SHORT).show()
    }

    @Inject
    lateinit var presenter: PasswordPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_password, container, false)

        AndroidSupportInjection.inject(this)
        presenter.attach(this)

        return view

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    fun save() {
        presenter.changePassword(editOldPassword.text.toString(), editNewPassword.text.toString())
    }
}
