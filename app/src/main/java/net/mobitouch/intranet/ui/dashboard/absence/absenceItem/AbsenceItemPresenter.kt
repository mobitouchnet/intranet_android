package net.mobitouch.intranet.ui.dashboard.absence.absenceItem

import net.mobitouch.intranet.model.AbsenceList
import net.mobitouch.intranet.model.DataResponse
import net.mobitouch.intranet.model.Requests
import net.mobitouch.intranet.repository.local.TokenDatabse
import net.mobitouch.intranet.repository.remote.Api
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class AbsenceItemPresenter @Inject constructor(val db:TokenDatabse, val retrofit: Retrofit){
    val api = retrofit.create(Api::class.java)
    lateinit var absenceItemView: AbsenceItemView

    fun attach(absenceItemView: AbsenceItemView){
        this.absenceItemView = absenceItemView
    }
//    fun getAbsenceItem(){
//        val token = db.tokenDao().getAll().last().token
//        api.getAbsence(token)
//            .enqueue(object : retrofit2.Callback<DataResponse<List<Requests>>>{
//                override fun onResponse(
//                    call: Call<DataResponse<List<Requests>>>,
//                    response: Response<DataResponse<List<Requests>>>
//                ) {
//                    response.body()?.data?.let {
//                        absenceItemView.showAbsence(it)
//                    }
//                }
//
//                override fun onFailure(call: Call<DataResponse<List<Requests>>>, t: Throwable) {
//
//                }
//            })
//    }
}