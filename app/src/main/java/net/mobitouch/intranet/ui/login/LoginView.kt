package net.mobitouch.intranet.ui.login

import net.mobitouch.intranet.R

interface LoginView {
    fun startHomeActivity()
    fun showErrorMessage(errorMessage: String)
    fun showInccorectLoginMessage(errorMessage: String)
    fun showInccorectLoginMessage(errorMessage: Int)
    fun showIncorectPasswordMessage(errorMessage: String)
}