package net.mobitouch.intranet.ui.dashboard

import dagger.Module
import dagger.android.ContributesAndroidInjector
import net.mobitouch.intranet.di.ActivityScope
import net.mobitouch.intranet.di.FragmentScope
import net.mobitouch.intranet.ui.dashboard.absence.MyAbsenceFragment
import net.mobitouch.intranet.ui.dashboard.card.CardFragment
import net.mobitouch.intranet.ui.dashboard.home.HomeFragment
import net.mobitouch.intranet.ui.dashboard.password.PasswordFragment
import net.mobitouch.intranet.ui.dashboard.profile.profile_fragment.ProfileFragment
import net.mobitouch.intranet.ui.dashboard.profile.profile_home.ProfileFragmentHome
import net.mobitouch.intranet.ui.dashboard.profile.profile_home.ProfileHomeFragment
import net.mobitouch.intranet.ui.dashboard.users.UsersFragment

@Module
internal abstract class DashboardActivityModule {
    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun homeFragment(): HomeFragment

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun usersFragment(): UsersFragment

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun myAbsenceFragment(): MyAbsenceFragment

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun bottomNavigationDrawerFragment(): BottomNavigationDrawerFragment

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun profileHomeFragment(): ProfileHomeFragment

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun cardFragment(): CardFragment

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun passwordFragment(): PasswordFragment

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun profilFragment(): ProfileFragment

}