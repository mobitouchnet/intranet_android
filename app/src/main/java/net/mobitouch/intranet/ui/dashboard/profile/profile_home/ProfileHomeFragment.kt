package net.mobitouch.intranet.ui.dashboard.profile.profile_home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TabHost
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayout
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_home_profile.*
import kotlinx.android.synthetic.main.fragment_home_profile.view.*
import net.mobitouch.intranet.R
import net.mobitouch.intranet.ui.dashboard.DashboardActivity
import net.mobitouch.intranet.ui.dashboard.card.CardFragment
import net.mobitouch.intranet.ui.dashboard.password.PasswordFragment
import net.mobitouch.intranet.ui.dashboard.profile.profile_fragment.ProfileFragment
import javax.inject.Inject


class ProfileHomeFragment : Fragment() {

    @Inject
    lateinit var profileFragment: ProfileFragment

    @Inject
    lateinit var cardFragment: CardFragment

    @Inject
    lateinit var passwordFragment: PasswordFragment

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home_profile, parent, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViewPager(viewPager)
        viewPager.offscreenPageLimit = 2
        val tabs = view.tabLayout as TabLayout
        tabs.setupWithViewPager(viewPager)
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = TabAdapter(childFragmentManager)
        adapter.addFragment(profileFragment, "Profil")
        adapter.addFragment(cardFragment, "Wizytówka")
        adapter.addFragment(passwordFragment, "Hasło")
        viewPager.adapter = adapter
    }

    fun save(){
        profileFragment.save()
        passwordFragment.save()
    }
}

