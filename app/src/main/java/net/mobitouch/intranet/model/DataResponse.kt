package net.mobitouch.intranet.model

data class DataResponse<T>(
        val status: Int,
        val message: String,
        val data:T
)