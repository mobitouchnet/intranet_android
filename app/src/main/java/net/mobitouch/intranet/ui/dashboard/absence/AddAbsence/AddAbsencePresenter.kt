package net.mobitouch.intranet.ui.dashboard.absence.AddAbsence

import android.widget.Spinner
import net.mobitouch.intranet.model.DataResponse
import net.mobitouch.intranet.repository.local.TokenDatabse
import net.mobitouch.intranet.repository.remote.Api
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class AddAbsencePresenter @Inject constructor(val db: TokenDatabse, val retrofit: Retrofit) {

    val api = retrofit.create(Api::class.java)

    lateinit var addAbsenceView: AddAbsenceView

    fun attach(addAbsenceView: AddAbsenceView) {
        this.addAbsenceView = addAbsenceView
    }
    fun addAbsence(dateFrom:String, dateTo:String, reason:String){
        val token = db.tokenDao().getAll().last().token
        api.postAbsence(token,dateFrom,dateTo,reason)
            .enqueue(object : Callback<DataResponse<Unit>>{
                override fun onResponse(call: Call<DataResponse<Unit>>, response: Response<DataResponse<Unit>>) {
                    if (response.code() == 201){
                        addAbsenceView.showCorrectMessage("Dodano wniosek")
                    }
                    else {
                        addAbsenceView.showErrorMessage("Error")
                    }
                }

                override fun onFailure(call: Call<DataResponse<Unit>>, t: Throwable) {
                    addAbsenceView.showCorrectMessage("Error")
                }
            })
    }


}