package net.mobitouch.intranet.model

import android.os.Parcel
import android.os.Parcelable

data class AbsenceList(
    val id: String,
    val date_from: String,
    val date_to: String,
    val days: Int,
    val status: String,
    val comment: Boolean,
    val reason: String,
    val file: String,
    val fileId: Boolean
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.readString(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(date_from)
        parcel.writeString(date_to)
        parcel.writeInt(days)
        parcel.writeString(status)
        parcel.writeByte(if (comment) 1 else 0)
        parcel.writeString(reason)
        parcel.writeString(file)
        parcel.writeByte(if (fileId) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AbsenceList> {
        override fun createFromParcel(parcel: Parcel): AbsenceList {
            return AbsenceList(parcel)
        }

        override fun newArray(size: Int): Array<AbsenceList?> {
            return arrayOfNulls(size)
        }
    }
}