package net.mobitouch.intranet.model

data class Statistics(
    val all: String,
    val yearly: String,
    val monthly: String,
    val weekly: String
)