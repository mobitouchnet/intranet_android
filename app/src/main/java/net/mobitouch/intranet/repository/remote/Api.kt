package net.mobitouch.intranet.repository.remote

import net.mobitouch.intranet.model.*
import retrofit2.Call
import retrofit2.http.*


interface
Api {

    @POST("auth/basic")
    @FormUrlEncoded
    fun authenticate(
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<DataResponse<Token>>

    @GET("user/profile")
    fun getUser(@Header("auth-token") token: String): Call<DataResponse<User>>

    @GET("absence/statistics")
    fun statistics(@Header("auth-token") token: String): Call<DataResponse<Statistics>>

    @GET("users")
    fun getUsersList(@Header("auth-token") token: String): Call<DataResponse<List<User>>>

    @GET("absence")
    fun getAbsence(@Header("auth-token") token: String): Call<DataResponse<List<Requests>>>

    @GET("card")
    fun getCard(@Header("auth-token") token: String): Call<DataResponse<Card>>

    @POST("user/password/change")
    @FormUrlEncoded
    fun changePassword(
        @Header("auth-token") token: String,
        @Field("old_password") oldPassword: String,
        @Field("new_password") newPassword: String
    ): Call<DataResponse<Unit>>

    @GET("user/profile")
    fun getUserById(@Header("auth-token") token: String): Call<DataResponse<User>>

    @POST("user/profile")
    @FormUrlEncoded
    fun postUser(
        @Header("auth-token") token: String,
        @FieldMap map: HashMap<String, Any>
    ): Call<DataResponse<String>>

    @POST("absence")
    @FormUrlEncoded
    fun postAbsence(
        @Header("auth-token") token: String,
        @Field("dateFrom") dateFrom: String,
        @Field("dateTo") dateTo: String,
        @Field("reason") reason: String
    ): Call<DataResponse<Unit>>

    @GET("user/profile/{id}")
    fun getUserItem(
        @Header("auth-token") token: String,
        @Path("id") id: String
    ): Call<DataResponse<User>>

}