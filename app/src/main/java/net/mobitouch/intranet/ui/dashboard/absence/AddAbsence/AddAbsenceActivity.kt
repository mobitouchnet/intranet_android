package net.mobitouch.intranet.ui.dashboard.absence.AddAbsence

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_add_absence.*
import kotlinx.android.synthetic.main.fragment_add_absence.view.*
import net.mobitouch.intranet.R
import javax.inject.Inject

class AddAbsenceActivity : AppCompatActivity(), AddAbsenceView {

    override fun showCorrectMessage(textMessage: String) {
        Toast.makeText(this, textMessage, Toast.LENGTH_SHORT).show()
    }

    override fun showErrorMessage(errorMessage: String) {
        Toast.makeText(this,errorMessage,Toast.LENGTH_SHORT).show()
    }

    @Inject
    lateinit var presenter: AddAbsencePresenter

    override fun onCreate (savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(net.mobitouch.intranet.R.layout.fragment_add_absence)
        AndroidInjection.inject(this)
        presenter.attach(this)


        val adapter = ArrayAdapter.createFromResource(
            this, R.array.reason_array,
            android.R.layout.simple_spinner_item
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        val dateFrom = editText as EditText
        val dateTo = editText2 as EditText
        buttonSend.setOnClickListener {
            var reason = when (spinner?.selectedItemId){
                1L -> "ABSENCE_REASON_SICKNESS"
                2L -> "ABSENCE_REASON_DELEGATION"
                3L -> "ABSENCE_REASON_CHILDCARE"
                4L -> "ABSENCE_REASON_REMOTE_WORK"
                5L -> "ABSENCE_REASON_UNPAID_LEAVE"
                6L -> "ABSENCE_REASON_MATERNITY_LEAVE"
                7L -> "ABSENCE_REASON_ON_DEMAND"
                8L -> "ABSENCE_REASON_PATERNITY_LEAVE"
                9L -> "ABSENCE_REASON_OCCASIONAL_LEAVE"
                10L -> "ABSENCE_REASON_PARENTAL_LEAVE"
                11L -> "ABSENCE_REASON_VACATION_LEAVE"

                else -> ""
            }

            presenter.addAbsence(dateFrom.text.toString(), dateTo.text.toString(), reason)
        }
    }




}