package net.mobitouch.intranet.di

import javax.inject.Scope

@Scope
annotation class SessionScope
