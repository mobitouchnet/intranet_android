package net.mobitouch.intranet.ui.dashboard.users.userItem

import net.mobitouch.intranet.model.User

interface UserItemView{
      fun showUser(it: User)
}